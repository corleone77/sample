#include <iostream>
#include <string>

using namespace std;

int count_a(string str)
{
  int retval = 0;
  
  for (int i = 0; i < (int)str.size(); ++i) {
    if (str[i] == 'A') {
      retval++;
    }
  }

  return retval;
}

int main()
{
  string str;
  
  cout << "please enter a string: ";
  cin >> str;

  cout << count_a(str) << endl;

  return 0;
}
