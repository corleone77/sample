#include <iostream>
#include <string>

using namespace std;

int search(string str, char c)
{
  for (int i = 0; i < (int)str.size(); ++i) {
    if (str[i] == c) {
      return i;
    }
  }
  return -1;
}

int main()
{
  string str;
  char c;

  cout << "please enter a string: ";
  cin >> str;
  cout << "the character to find: ";
  cin >> c;

  int pos = search(str, c);
  
  if (pos < 0)
    cout << "No\n";
  else
    cout << pos << endl;

  return 0;
}
