#include <iostream>
#include <string>

using namespace std;

bool is_palindrome(string str)
{
  int l = 0, r = (int)str.size()-1;
  while (l < r) {
    if (str[l] != str[r]) {
      return false;
    }

    l++;
    r--;
  }
  return true;
}

int main()
{
  string str;
  
  cout << "please enter a string: ";
  cin >> str;

  if (is_palindrome(str))
    cout << "Yes\n";
  else
    cout << "No\n";

  return 0;
}
